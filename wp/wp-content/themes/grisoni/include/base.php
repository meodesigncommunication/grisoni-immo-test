<?php

global $wpdb;

define('DB_USER_EXT','externalPE');
define('DB_HOST_EXT','mysql.pierreetoile.ch');
define('DB_NAME_EXT','pierreetoilech');
define('DB_PASSWORD_EXT','f11PCMjcoRwu');

$post = new TimberPost();
$context = Timber::get_context();
$context['site_url'] = get_site_url();
$context['post'] = $post;
$context['menus'] = wp_nav_menu(array('menu' => 'main', 'echo' => false));

$context['menu'] = new TimberMenu('main');

/*
 *  Include post Thumb
 */
$context['featured_image'] = (get_the_post_thumbnail( $post->ID, 'full' )) ? get_the_post_thumbnail( $post->ID, 'full' ) : '';

/*
 *  Include theme path
 */
$context['template_path'] = get_template_directory();
$context['template_path_uri'] = get_template_directory_uri();

/*
 *  Include meta Data in page
 */
$context['meta_title'] = get_field('meta_title', $post->ID);
$context['meta_description'] = get_field('meta_description', $post->ID);
$context['meta_keywords'] = get_field('meta_keywords', $post->ID);

/*
 *  Include page content in page
 */
$context['page_content'] = get_field('page_content', $post->ID);

/*
 *  Include Broker in page footer
 */
$context['broker_full_name'] = get_field('full_name', 'option');
$context['broker_phone'] = get_field('phone', 'option');
$context['broker_email'] = get_field('email', 'option');
$context['broker_picture'] = get_field('picture', 'option');
$context['broker_poste'] = get_field('poste', 'option');

/*
 *  Include other promotions in page footer
 */
$promotion_1 = get_field('promotion_1', 'option');
$promotion_2 = get_field('promotion_2', 'option');
$promotion_3 = get_field('promotion_3', 'option');

/*
 *  Include ficher header
 */
$context['info_header'] = (get_field('info_header', 'option') != 'disabled') ? true : false ;
$context['info_top_line'] = get_field('info_top_line', 'option');
$context['info_middle_line'] = get_field('info_middle_line', 'option');
$context['info_bottom_line'] = get_field('info_bottom_line', 'option');

/*
$pe_db = new wpdb(DB_USER_EXT, DB_PASSWORD_EXT, DB_NAME_EXT, DB_HOST_EXT);
$pe_db->show_errors();
*/
/*$query = " SELECT *
           FROM wp_posts AS p 
           WHERE p.ID = 1061 ";

$promotions = $pe_db->get_results($query);*/

/*
 *  Include availability apartements
 */
$context['status_popup'] = (get_field('status_popup','option') == 'enabled') ? true : false;
$context['total_disponible_popup'] = get_field('total_disponible_popup','option');

/*
 *  Include project image header
 */
$context['promo_logo'] = get_field('promo_logo','option'); // Promotion logo
$context['promo_logo_interne'] = get_field('promo-logo-interne','option'); // Promotion logo
$context['status_img_header'] = (get_field('status_img_header','option') == 'enabled') ? true : false;
$context['image_header'] = get_field('image_header','option');