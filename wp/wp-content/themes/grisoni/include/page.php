<?php

/*
 * Slideshow
 *
 */
$context['status_slider'] = (get_field('status_slider', $post->ID) == 'enabled') ? true : false;
$context['images_slider'] = get_field('images_slider', $post->ID);