<?php
/*
 * Template name: TPL Homepage
 */

require_once 'include/base.php';

Timber::render( 'templates/layouts/header.html.twig' , $context );
Timber::render( 'templates/homepage.html.twig' , $context );
Timber::render( 'templates/layouts/footer.html.twig' , $context );