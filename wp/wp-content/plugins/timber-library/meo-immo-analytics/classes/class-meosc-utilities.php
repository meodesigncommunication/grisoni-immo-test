<?php
/*
Copyright (C) MEO design et communication Sàrl 2014. All rights reserved

Unauthorized copying of this file via any medium is strictly prohibited
Proprietary and confidential

info@allmeo.com
*/


class MeoScCf7Utilities {

	public function __construct() {
	}

	# Find PostID based on the template name (limit 1)
	public function getPostIdForTemplate($template_file) {
		global $wpdb;

		$sql = "SELECT pm.post_id
		        	FROM ".$wpdb->prefix."postmeta pm
		         	WHERE pm.meta_key = '_wp_page_template' AND pm.meta_value = %s
		         		LIMIT 1";

		$sql = $wpdb->prepare($sql, $template_file);

		return $wpdb->get_var($sql);
	}

	public function escapeHtml($in) {
		$translation_table = get_html_translation_table( HTML_ENTITIES, ENT_NOQUOTES );
		$translation_table[chr(38)] = '&';
		unset($translation_table['<']);
		unset($translation_table['>']);

		return strtr($in, $translation_table);
	}
}
